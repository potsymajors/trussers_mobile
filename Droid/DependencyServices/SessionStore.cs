﻿using Xamarin.Forms;
using Trussers.Droid;
using Android.Content;
using Android.Preferences;
using Newtonsoft.Json;

[assembly: Dependency(typeof(SessionStore))]
namespace Trussers.Droid
{
	public class SessionStore : ITokenStore
	{
		ISharedPreferences mSharedPrefs;
		ISharedPreferencesEditor mPrefsEditor;
		Context mContext;

		public SessionStore()
		{
			mContext = Forms.Context;
			mSharedPrefs = PreferenceManager.GetDefaultSharedPreferences(mContext);
			mPrefsEditor = mSharedPrefs.Edit();
		}

		public string GetString(string key, string defValue)
		{
			return mSharedPrefs.GetString(key, defValue);
		}

		public int GetInt(string key, int defValue)
		{
			return mSharedPrefs.GetInt(key, defValue);
		}

		public void Dispose()
		{
			mSharedPrefs.Dispose();
		}

		public void Apply()
		{
			mPrefsEditor.Apply();
		}

		public ISharedPreferencesEditor Clear()
		{
			mPrefsEditor.Clear();
			Commit();
			Apply();
			return mPrefsEditor;
		}

		public bool Commit()
		{
			return mPrefsEditor.Commit();
		}

		public ISharedPreferencesEditor PutInt(string key, int value)
		{
			var ret = mPrefsEditor.PutInt(key, value);
			Commit();
			Apply();
			return ret;
		}

		public ISharedPreferencesEditor PutString(string key, string value)
		{
			var ret = mPrefsEditor.PutString(key, value);
			Commit();
			Apply();
			return ret;
		}

		public void StoreContractor(Contractor authToken)
		{
			var json = JsonConvert.SerializeObject(authToken);
			PutString(AppPropertyHelper.TokenKey, json);
		}

		public Contractor RetrieveContractor()
		{
			var json = GetString(AppPropertyHelper.TokenKey, "");

			if (string.IsNullOrEmpty(json))
			{
				return null;
			}

			return JsonConvert.DeserializeObject<Contractor>(json);
		}

		public void ClearContractor()
		{
			Clear();
		}
	}
}