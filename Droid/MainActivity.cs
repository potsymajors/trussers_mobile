﻿using System;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using FFImageLoading.Forms.Droid;
using XLabs.Ioc;
using XLabs.Platform.Device;
using ImageCircle.Forms.Plugin.Droid;
using Acr.UserDialogs;

namespace Trussers.Droid
{
	[Activity(Label = "Trussers", Icon = "@drawable/icon", Theme = "@style/MyTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize, ScreenOrientation = ScreenOrientation.Portrait)]
	public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
	{
		protected override void OnCreate(Bundle bundle)
		{
			TabLayoutResource = Resource.Layout.Tabbar;
			ToolbarResource = Resource.Layout.Toolbar;

			base.OnCreate(bundle);

			global::Xamarin.Forms.Forms.Init(this, bundle);
			CachedImageRenderer.Init();
			ImageCircleRenderer.Init();
			UserDialogs.Init(this);

			if (!Resolver.IsSet)
			{
				SetIoc();
			}

			LoadApplication(new App());
		}

		void SetIoc()
		{
			var resolverContainer = new SimpleContainer();
			resolverContainer.Register<IDevice>(t => AndroidDevice.CurrentDevice).Register<IDisplay>(t => t.Resolve<IDevice>().Display);
			Resolver.SetResolver(resolverContainer.GetResolver());
		}
	}
}