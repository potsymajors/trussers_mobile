﻿using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Trussers;
using Trussers.Droid;

[assembly: ExportRenderer(typeof(TrussersLabel), typeof(TrussersLabelRenderer))]
namespace Trussers.Droid
{
	public class TrussersLabelRenderer : LabelRenderer
	{
		TrussersLabel FormsControl;

		protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
		{
			base.OnElementChanged(e);

			if (Element != null)
			{
				FormsControl = Element as TrussersLabel;
			}

			if (Control != null)
			{
				
			}
		}

		protected override void OnElementPropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			base.OnElementPropertyChanged(sender, e);
		}
	}
}