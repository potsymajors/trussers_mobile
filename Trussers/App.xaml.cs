﻿using System.Linq;
using Xamarin.Forms;
using XLabs.Forms.Mvvm;

namespace Trussers
{
	public partial class App : Application
	{
		public static TrussersMasterDetailPage MainPageContainer;

		public App()
		{
			InitializeComponent();

			ViewFactoryHelper.RegisterViews();
			MainPage = new SplashPage();

			var appEntryService = new AppService();
			appEntryService.AttemptAutoLogin();
		}

		public void SetTopPage(Page page)
		{
			Device.BeginInvokeOnMainThread(() => Application.Current.MainPage = page);
		}

		public void ShowLoginPage()
		{
			var loginPage = ViewFactory.CreatePage<LoginViewModel, LoginPage>() as Page;
			SetTopPage(new NavigationPage(loginPage));
		}

		protected override void OnStart()
		{
			// Handle when your app starts
		}

		protected override void OnSleep()
		{
			Session.Instance().AllowBackButtonOnTheMap = false;
		}

		protected override void OnResume()
		{
			var lastPage = TrussersMasterDetailPage.MasterNavPage.Navigation.NavigationStack.Last();

			if (lastPage.GetType() == typeof(HomePage))
			{
				((HomePage)lastPage).RefreshData();
			}

		}
	}
}
