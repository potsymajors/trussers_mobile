﻿using Acr.UserDialogs;
using Plugin.Connectivity;
using Xamarin.Forms;
using XLabs.Forms.Mvvm;
using System.Reactive.Linq;
using System;

namespace Trussers
{
	public class AppService
	{
		public void GoToMainPage()
		{
			if (!CrossConnectivity.Current.IsConnected)
			{
				UserDialogs.Instance.Toast("No internet connection");
				return;
			}

			var page = new TrussersMasterDetailPage();
			App.MainPageContainer = page;
			SetTopPage(page);
		}

		public void SetTopPage(Page page)
		{
			Device.BeginInvokeOnMainThread(() => Application.Current.MainPage = page);
		}

		public void GoToWelcomePage()
		{
			var welcomePage = ViewFactory.CreatePage<LoginViewModel, LoginPage>() as Page;
			SetTopPage(new NavigationPage(welcomePage));
		}

		public void AttemptAutoLogin()
		{
			var contractor = AppPropertyHelper.GetContractor();

			if (Session.Instance().IsLoggedIn)
			{
				Session.Instance().Contractor = contractor;
				GoToMainPage();
				GetUser(contractor);
			}
			else
			{
				GoToWelcomePage();
			}
		}

		public void GetUser(Contractor contractor)
		{
			var service = new ContractorService();
			service.GetContractorDetails(contractor).Subscribe(
				contractorDetails =>
				{
					contractor.Username = contractorDetails.Username;					
					AppPropertyHelper.PutContractor(contractor);
					Session.Instance().Contractor = contractor;					
				},
				error =>
				{
						
				},
				onCompleted: () =>
				{
					
				}
			);
		}

	}
}
