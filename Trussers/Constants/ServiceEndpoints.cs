namespace Trussers
{
	public static class ServiceEndpoints
	{
		#region Login Services

		public static string LOGIN = "Login/Login/";
		public static string CONTRACTOR_DETAILS = "Contractor/GetContractor/";
		public static string LOGOUT = "Login/Logout/";
		public static string SESSION_KEY_STATUS = "SessionKey/GetSessionKeyStatus";

		#endregion

		#region Gig Services

		public static string GIG_HEADER = "Gig/GetCtrGigHeader";
		public static string OPEN_GIGS = "Gig/GetOpenGigs";
		public static string GIG_HISTORY = "Gig/GetGigHistory";
		public static string GIG_DETAILS = "Gig/GetGigDetails";
		public static string SCHEDULE_GIG = "Gig/ScheduleGig";
		public static string FORFEIT_GIG = "Gig/ForfeitGig";
		public static string START_TRAVEL = "Gig/StartTravel";
		public static string START_WORKING = "Gig/StartWorking";
		public static string COMPLETE_GIG = "Gig/CompleteGig";

		#endregion

		#region Contractor Services

		public static string UPDATE_CONTRACTOR = "Contractor/UpdateContractor";
  		public static string GET_CONTRACTOR = "Contractor/GetContractor";

		#endregion
	}
}