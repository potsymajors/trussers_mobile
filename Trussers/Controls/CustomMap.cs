﻿using System.Collections.Generic;
using Xamarin.Forms.Maps;

namespace Trussers
{
	public class CustomMap : Map
	{
		public List<CustomPin> CustomPins = new List<CustomPin>();
	}
}
