﻿using Xamarin.Forms.Maps;

namespace Trussers
{
	public class CustomPin
	{
		public Pin Pin { get; set; }
		public string Id { get; set; }
		public bool HomeStation { get; set; }
	}
}