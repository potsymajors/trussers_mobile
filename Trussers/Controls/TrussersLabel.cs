﻿using Xamarin.Forms;

namespace Trussers
{
	public class TrussersLabel : Label
	{
		public static readonly BindableProperty BorderColorProperty =
			BindableProperty.Create("BorderColor", typeof(Color), typeof(TrussersLabel), Color.Black);
		
		public Color BorderColor
		{
			get { return (Color)GetValue(BorderColorProperty); }
			set { SetValue(BorderColorProperty, value); }
		}

		public static readonly BindableProperty ImageSourceProperty =
			BindableProperty.Create("ImageSource", typeof(ImageSource), typeof(TrussersLabel), null);

		public ImageSource ImageSource
		{
			get { return (ImageSource)GetValue(ImageSourceProperty); }
			set { SetValue(ImageSourceProperty, value); }
		}
	}
}