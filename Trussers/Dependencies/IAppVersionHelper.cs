namespace Trussers
{
	public interface IAppVersionHelper
	{
		string AppVersion { get; }
	}
}