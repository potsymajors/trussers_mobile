namespace Trussers
{
	public interface ITokenStore
	{
		void StoreContractor (Contractor sessionKey);
		Contractor RetrieveContractor ();
		void ClearContractor();
	}
}