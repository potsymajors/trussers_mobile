﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Trussers
{
	public enum AccountStatus
	{
	}

	public class AccountStatusHelper : BaseEnumHelper<AccountStatus>
	{
		public override AccountStatus GetFromName(string name)
		{
			switch (name)
			{
				
			}

			throw new ValidationException("name (" + name + ") didn't match any AccountStatus enum item");
		}
	}
}
