﻿using System.ComponentModel.DataAnnotations;

namespace Trussers
{
	public enum ClientContractorStatus
	{
		[Display(Name = "Test")]
		Test,

		[Display(Name = "TBD")]
		TBD
	}

	public class ClientContractStatusHelper : BaseEnumHelper<ClientContractorStatus>
	{
		public override ClientContractorStatus GetFromName(string name)
		{
			switch (name)
			{

			}

			throw new ValidationException("name (" + name + ") didn't match any ClientContractorStatus enum item");
		}
	}
}
