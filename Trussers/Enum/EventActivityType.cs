﻿using System.ComponentModel.DataAnnotations;

namespace Trussers
{
	public enum EventActivityType
	{
	}

	public class EventActivityTypeHelper : BaseEnumHelper<EventActivityType>
	{
		public override EventActivityType GetFromName(string name)
		{
			switch (name)
			{

			}

			throw new ValidationException("name (" + name + ") didn't match any EventActivityType enum item");
		}
	}
}
