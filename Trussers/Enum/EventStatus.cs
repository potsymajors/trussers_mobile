﻿using System.ComponentModel.DataAnnotations;

namespace Trussers
{
	public enum EventStatus
	{		
	}

	public class EventStatusHelper : BaseEnumHelper<EventStatus>
	{
		public override EventStatus GetFromName(string name)
		{
			switch (name)
			{

			}

			throw new ValidationException("name (" + name + ") didn't match any EventStatus enum item");
		}
	}
}
