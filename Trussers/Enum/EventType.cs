﻿using System.ComponentModel.DataAnnotations;

namespace Trussers
{
	public enum EventType
	{

	}

	public class EventTypeHelper : BaseEnumHelper<EventType>
	{
		public override EventType GetFromName(string name)
		{
			switch (name)
			{

			}

			throw new ValidationException("name (" + name + ") didn't match any EventType enum item");
		}
	}
}
