﻿using System.ComponentModel.DataAnnotations;

namespace Trussers
{
	public enum PositionType
	{

	}

	public class PositionTypeHelper : BaseEnumHelper<PositionType>
	{
		public override PositionType GetFromName(string name)
		{
			switch (name)
			{

			}

			throw new ValidationException("name (" + name + ") didn't match any PositionType enum item");
		}
	}
}
