﻿using System;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace Trussers
{
	public static class EnumExtensions
	{
		public static string GetStringValue<T> (this T enumValue)
		{
			var label = Convert.ToString (enumValue);
			var ti = typeof (T).GetTypeInfo ();
			var property = ti.DeclaredMembers.First (x => x.Name == label);
			var attribute = CustomAttributeExtensions.GetCustomAttribute<DisplayAttribute> (property);

			if (attribute != null)
			{
				return attribute.Name;
			}

			return label;
		}
	}
}
