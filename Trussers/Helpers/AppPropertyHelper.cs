using Xamarin.Forms;

namespace Trussers
{
	public static class AppPropertyHelper
	{
		public const string TokenKey = "Session_key";

		public static Contractor GetContractor () => DependencyService.Get<ITokenStore> ().RetrieveContractor ();

		public static void PutContractor (Contractor authToken) => DependencyService.Get<ITokenStore> ().StoreContractor (authToken);

		public static string GetVersion() => DependencyService.Get<IAppVersionHelper>().AppVersion;

		public static string GetDeviceOS()
		{			
			return Device.OS.ToString();
		}

		public static string GetDeviceType()
		{
			return Device.Idiom.ToString();
		}
	}
}