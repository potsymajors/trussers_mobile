﻿namespace Trussers
{
	public abstract class BaseEnumHelper<TEnum> where TEnum : struct
	{
		public abstract TEnum GetFromName (string name);
	}
}