using Xamarin.Forms;

namespace Trussers
{
	public class DeviceHelper
	{
		public static bool IsDeviceIPhone5OrEarlier()
		{
			return (Device.OS == TargetPlatform.iOS && Application.Current.MainPage.Width <= 320);
		}

		public static float GetDeviceWidth()
		{
			if (Application.Current != null && Application.Current.MainPage != null)
			{
				return (float)Application.Current.MainPage.Width;
			}
			return 0.0f;
		}

		public static float GetDeviceHeight()
		{
			if (Application.Current != null && Application.Current.MainPage != null)
			{
				return (float)Application.Current.MainPage.Height;
			}
			return 0.0f;
		}
	}
}