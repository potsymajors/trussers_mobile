using XLabs.Forms.Mvvm;

namespace Trussers
{
	public static class ViewFactoryHelper
	{
		public static void RegisterViews()
		{
			ViewFactory.Register<LoginPage, LoginViewModel>();
			ViewFactory.Register<TrussersMasterPage, TrussersMasterViewModel>();
			ViewFactory.Register<AccountPage, AccountViewModel>();
			ViewFactory.Register<HistoryPage, HistoryViewModel>();
			ViewFactory.Register<LegalInfoPage, LegalInfoViewModel>();
			ViewFactory.Register<SupportPage, SupportViewModel>();
			ViewFactory.Register<AdditionalTermsPage, AdditionalTermsViewModel>();
			ViewFactory.Register<ContractorTermsOfService, ContractorTermsViewModel>();
			ViewFactory.Register<HomePage, HomeViewModel>();
			ViewFactory.Register<AvailableGigsPage, AvailableGigsViewModel>();
			ViewFactory.Register<GigDetailPage, GigDetailViewModel>();
			ViewFactory.Register<StartRoutePage, StartRouteViewModel>();
			ViewFactory.Register<InProgressPage, InProgressViewModel>();
			ViewFactory.Register<AvailableGigDetailsPage, AvailableGigDetailsViewModel>();
			ViewFactory.Register<LearnMorePage, LearnMoreViewModel>();
			ViewFactory.Register<DirectionsPage, DirectionsViewModel>();
		}
	}
}