﻿using System;
using Newtonsoft.Json;

namespace Trussers
{
	public class EnumConverter<TEnum, THelper> : JsonConverter
		where TEnum : struct
		where THelper : BaseEnumHelper<TEnum>, new()
	{
		public override bool CanRead { get { return true; } }
		public override bool CanWrite { get { return true; } }

		public override void WriteJson (JsonWriter writer, object value, JsonSerializer serializer)
		{
			var valueAsEnum = (TEnum)value;
			writer.WriteValue (valueAsEnum.GetStringValue ());
		}

		public override object ReadJson (JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			var enumString = (string)reader.Value;
			var helper = new THelper ();

			return helper.GetFromName (enumString);
		}

		public override bool CanConvert (Type objectType)
		{
			return objectType == typeof (string);
		}
	}
}
