﻿using Newtonsoft.Json;

namespace Trussers
{
	public class Contractor : EntityBase
	{
		[JsonProperty("ContractorID")]
		public int Identifier;

		[JsonProperty("AllowLogin")]
		public int AllowLogin;

		[JsonProperty("LoginMessage")]
		public string LoginMessage;

		[JsonProperty("SessionKey")]
		public string SessionKey { get; set; }

		[JsonProperty("FirstName")]
		public string FirstName { get; set; }

		[JsonProperty("LastName")]
		public string LastName { get; set; }

		[JsonProperty("Email")]
		public string Email { get; set; }

		[JsonProperty("CellNumber")]
		public string CellNumber { get; set; }

		[JsonProperty("IDPhotoURL")]
		public string AvatarUrl { get; set; }

		[JsonProperty("Username")]
		public string Username { get; set; }

		[JsonProperty("OldPassword")]
		public string OldPassword { get; set; }

		[JsonProperty("NewPassword")]
		public string NewPassword { get; set; }
	}
}