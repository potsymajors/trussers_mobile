﻿using Newtonsoft.Json;

namespace Trussers
{
	public class ContractorInformation : EntityBase
	{
		[JsonProperty("SessionKeyStatusID")]
		public string SessionKeyStatusID { get; set; }

		[JsonProperty("ID")]
		public int ID { get; set; }

		[JsonProperty("Value")]
		public string Value { get; set; }
	}
}