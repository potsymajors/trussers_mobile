﻿using Newtonsoft.Json;

namespace Trussers
{
	public class ContractorUpdate
	{
		[JsonProperty("InContractorSessionKey")]
		public InContractorSessionKey InContractorSessionKey { get; set; }

		[JsonProperty("InContractor")]
		public Contractor Contractor { get; set; }
	}
}