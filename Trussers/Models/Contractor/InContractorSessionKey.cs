﻿using System;
using Newtonsoft.Json;

namespace Trussers
{
	public class InContractorSessionKey
	{
		[JsonProperty("ContractorID")]
		public int ContractorID { get; set; }

		[JsonProperty("SessionKey")]
		public string SessionKey { get; set; }
	}
}