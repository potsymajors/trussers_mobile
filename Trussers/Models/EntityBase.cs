﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Trussers
{
	public class EntityBase
	{
		[JsonProperty("id")]
		public int Id { get; set; }

		[JsonProperty("name")]
		public string Name { get; set; }

		public override bool Equals(object obj)
		{
			return obj is EntityBase && Id == ((EntityBase)obj).Id;
		}

		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
	}

	public class EntityBaseComparer<T> : IEqualityComparer<T> where T : EntityBase
	{
		public bool Equals(T x, T y)
		{
			return x.Id == y.Id;
		}

		public int GetHashCode(T obj)
		{
			return obj.GetHashCode();
		}
	}
}