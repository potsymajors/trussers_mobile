﻿using System;
using Newtonsoft.Json;

namespace Trussers
{
	public class Gig : EntityBase
	{
		[JsonProperty("GigID")]
		public string Identifier { get; set; }

		[JsonProperty("StartDate")]
		public DateTime StartDate { get; set; }

		[JsonProperty("Location")]
		public string Location { get; set; }

		[JsonProperty("Description")]
		public string Description { get; set; }

		[JsonProperty("Position")]
		public string Position { get; set; }

		[JsonProperty("Pay")]
		public string Pay { get; set; }

		[JsonProperty("EventName")]
		public string Event { get; set; }

		public string StartDateString { get; set; }
		public string StartTimeString { get; set; }
	}
}