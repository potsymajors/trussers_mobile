﻿using System;
using Newtonsoft.Json;

namespace Trussers
{
	public class GigDetails : EntityBase
	{
		[JsonProperty("GigID")]
		public string Identifier { get; set; }

		[JsonProperty("StartDate")]
		public DateTime StartDate { get; set; }

		[JsonProperty("ScheduledDate")]
		public DateTime ScheduledDate { get; set; }

		[JsonProperty("CheckInDate")]
		public DateTime CheckInDate { get; set; }

		[JsonProperty("CompleteDate")]
		public DateTime CompleteDate { get; set; }

		[JsonProperty("Location")]
		public string Location { get; set; }

		[JsonProperty("Position")]
		public string Position { get; set; }

		[JsonProperty("Pay")]
		public string Pay { get; set; }

		[JsonProperty("Description")]
		public string Description { get; set; }

		[JsonProperty("ArrivalInstructions")]
		public string ArrivalInstructions { get; set; }

		[JsonProperty("ContactName")]
		public string ContactName { get; set; }

		[JsonProperty("ContactCell")]
		public string ContactCell { get; set; }

		[JsonProperty("LocationDetail")]
		public Location LocationDetail { get; set; }

		[JsonProperty("EventType")]
		public string EventType { get; set; }

		[JsonProperty("DrivingDirections")]
		public string DrivingDirections { get; set; }
	}
}
