﻿using Newtonsoft.Json;

namespace Trussers
{
	public class GigHeader : EntityBase
	{
		public GigHeader()
		{
			ScheduledGig = new ScheduledGigInformation();
		}

		[JsonProperty("username")]
		public string Username { get; set; }

		[JsonProperty("SessionKeyStatusID")]
		public string SessionKeyStatusID { get; set; }

		[JsonProperty("ScheduledGigInfo")]
		public ScheduledGigInformation ScheduledGig { get; set; }

		[JsonProperty("OpenGigInfo")]
		public OpenGigInformation OpenGig { get; set; }

		public bool AreThereScheduledGigs()
		{
			return ScheduledGig?.ScheduledGigs?.Count > 0;
		}
	}
}