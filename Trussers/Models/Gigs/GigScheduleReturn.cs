﻿using Newtonsoft.Json;

namespace Trussers
{
	public class GigScheduleReturn: EntityBase
	{
		[JsonProperty("SessionKeyStatusID")]
		public int SessionKeyStatusID { get; set; }

		[JsonProperty("ScheduleStatusID")]
		public int ScheduleStatusID { get; set; }

		[JsonProperty("ScheduleStatusMessage")]
		public string ScheduleStatusMessage { get; set; }
	}
}