﻿using Newtonsoft.Json;

namespace Trussers
{
	public class OpenGigInformation
	{
		[JsonProperty("GigsAvailable")]
		public int GigsAvailable { get; set; }

		[JsonProperty("ShortStatus")]
		public string ShortStatus { get; set; }

		[JsonProperty("FullStatus")]
		public string FullStatus { get; set; }
	}
}