﻿using Newtonsoft.Json;

namespace Trussers
{
	public class ScheduleGig: EntityBase
	{
		[JsonProperty("GigID")]
		public int GigID { get; set; }

		[JsonProperty("ContractorID")]
		public int ContractorID { get; set; }

		[JsonProperty("SessionKey")]
		public string SessionKey { get; set; }
	}
}