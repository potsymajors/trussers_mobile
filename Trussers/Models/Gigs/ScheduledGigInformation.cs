﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Trussers
{
	public class ScheduledGigInformation
	{
		[JsonProperty("InProgressGig")]
		public int InProgressGig { get; set; }

		[JsonProperty("PendingGig")]
		public int PendingGig { get; set; }

		[JsonProperty("EnrouteGig")]
		public int EnrouteGig { get; set; }

		[JsonProperty("ScheduledGigs")]
		public List<Gig> ScheduledGigs { get; set;}
	}
}
