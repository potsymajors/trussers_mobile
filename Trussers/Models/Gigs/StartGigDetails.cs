﻿using Newtonsoft.Json;

namespace Trussers
{
	public class StartGigDetails : EntityBase
	{
		[JsonProperty("ScheduleStatusMessage")]
		public string ScheduleStatusMessage { get; set; }

		[JsonProperty("GigDetails")]
		public GigDetails GigDetails { get; set; }
	}
}