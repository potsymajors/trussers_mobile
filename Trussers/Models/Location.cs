﻿using Newtonsoft.Json;

namespace Trussers
{
	public class Location : EntityBase
	{
		[JsonProperty("LocationID")]
		public int LocationID { get; set; }

		[JsonProperty("Name")]
		public string Name { get; set; }

		[JsonProperty("Address")]
		public string Address { get; set; }

		[JsonProperty("City")]
		public string City { get; set; }

		[JsonProperty("State")]
		public string State { get; set; }

		[JsonProperty("Zip")]
		public string Zip { get; set; }

		[JsonProperty("Lat")]
		public double Lat { get; set; }

		[JsonProperty("Lon")]
		public double Lon { get; set; }

		public override string ToString()
		{
			return Address + "\n" + City + ", " + State + "\n" + Zip;
		}
	}
}