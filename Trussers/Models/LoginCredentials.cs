﻿using Newtonsoft.Json;

namespace Trussers
{
	public class LoginCredentials: EntityBase
	{
		[JsonProperty("UserName")]
		public string Username { get; set; }

		[JsonProperty("Password")]
		public string Password { get; set; }
	}
}