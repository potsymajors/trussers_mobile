﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace Trussers
{
	public class SessionKey : EntityBase
	{
		[JsonProperty("SessionKey")]
		public string SessionToken { get; set; }

		[JsonProperty("Gigs")]
		public List<Gig> Gigs { get; set; }
	}
}