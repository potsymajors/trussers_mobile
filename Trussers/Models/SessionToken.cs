﻿using System;
using Newtonsoft.Json;

namespace Trussers
{
	public class SessionToken
	{
		[JsonProperty("expires_in")]
		public DateTime ExpiresOn { get; set; }

		[JsonProperty("access_token")]
		public string Token { get; set; }

		[JsonIgnore]
		public bool IsValid => !string.IsNullOrEmpty(Token) && DateTime.Compare(DateTime.UtcNow, ExpiresOn) < 1;
	}
}
