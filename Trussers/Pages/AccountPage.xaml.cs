﻿using Xamarin.Forms;

namespace Trussers
{
	public partial class AccountPage : BaseView
	{
		public AccountPage()
		{
			InitializeComponent();
			NavigationPage.SetHasNavigationBar(this, true);
		}
	}
}