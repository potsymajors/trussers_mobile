using Plugin.Connectivity;
using Plugin.Connectivity.Abstractions;
using Xamarin.Forms;

namespace Trussers
{
	public class BaseView : XLabs.Forms.Mvvm.BaseView
	{
		public bool HasNavBar;
		public bool IsOffline;
		public bool ShouldShowBackButton;
		public string NameOfView;

		public View TitleView { get; set; }

		ToolbarItem SearchToolbarItem, CartToolbarItem;

		public BaseView()
		{
			NameOfView = GetType().ToString();
			HasNavBar = true;
			ShouldShowBackButton = true;
			IsOffline = !CrossConnectivity.Current.IsConnected;
		}

		protected override void OnAppearing()
		{
			base.OnAppearing ();
			CrossConnectivity.Current.ConnectivityChanged += ConnectivityChanged;
		}

		protected override void OnDisappearing()
		{
			base.OnDisappearing ();
			CrossConnectivity.Current.ConnectivityChanged -= ConnectivityChanged;
		}

		public virtual void ConnectivityChanged(object sender, ConnectivityChangedEventArgs e)
		{
			IsOffline = !e.IsConnected;
		}

		#region Navigation

		public void PushToPage (Page page)
		{
			Device.BeginInvokeOnMainThread (async () => await Navigation.PushAsync (page));
		}

		#endregion

		#region Toolbar

		public void ShowDefaultToolbarItems ()
		{
			if (!ToolbarItems.Contains (SearchToolbarItem))
			{
				ToolbarItems.Add (SearchToolbarItem);
			}
			if (!ToolbarItems.Contains (CartToolbarItem))
			{
				ToolbarItems.Add (CartToolbarItem);
			}
		}

		public void AddToolbarItem (ToolbarItem item, bool clearToolbar = true)
		{
			if (clearToolbar)
			{
				ClearToolbar ();
			}

			ToolbarItems.Add (item);
		}

		public void AddToolbarItems (bool clearToolbar, params ToolbarItem [] items)
		{
			if (clearToolbar)
			{
				ClearToolbar ();
			}

			foreach (var item in items)
			{
				ToolbarItems.Add (item);
			}
		}

		public void ClearToolbar ()
		{
			ToolbarItems.Clear ();
		}

		#endregion
	}
}