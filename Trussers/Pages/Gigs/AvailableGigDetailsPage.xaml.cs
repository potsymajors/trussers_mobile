﻿namespace Trussers
{
	public partial class AvailableGigDetailsPage : BaseView
	{
		public AvailableGigDetailsPage()
		{
			InitializeComponent();
			Title = "Available Gigs";
		}
	}
}