﻿using Xamarin.Forms;

namespace Trussers
{
	public partial class AvailableGigsPage : BaseView
	{
		public AvailableGigsPage()
		{
			InitializeComponent();
			Title = "Available Gigs";
			NavigationPage.SetBackButtonTitle(this, string.Empty);
			NavigationPage.SetHasBackButton(this, false);

			ListView.ItemSelected += (sender, e) =>
			{
				ListView.SelectedItem = null;
			};
		}
	}
}