﻿using Xamarin.Forms;

namespace Trussers
{
	public partial class GigDetailPage : BaseView
	{
		public GigDetailPage()
		{
			InitializeComponent();
			Title = "Scheduled Gigs Detail";
			NavigationPage.SetBackButtonTitle(this, string.Empty);
			NavigationPage.SetHasBackButton(this, false);
		}
	}
}