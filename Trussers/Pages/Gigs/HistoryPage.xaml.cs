﻿using Xamarin.Forms;

namespace Trussers
{
	public partial class HistoryPage : BaseView
	{
		public HistoryPage()
		{
			InitializeComponent();
			NavigationPage.SetHasNavigationBar(this, true);

			ListView.ItemSelected += (sender, e) =>
			{
				ListView.SelectedItem = null;
			};
		}
	}
}