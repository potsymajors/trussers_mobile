﻿using Xamarin.Forms;

namespace Trussers
{
	public partial class InProgressPage : BaseView
	{
		public InProgressPage()
		{
			InitializeComponent();
			Title = "In Progress";
			NavigationPage.SetBackButtonTitle(this, string.Empty);
			NavigationPage.SetHasBackButton(this, false);
		}
	}
}