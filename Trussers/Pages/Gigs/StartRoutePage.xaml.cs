﻿using Xamarin.Forms;

namespace Trussers
{
	public partial class StartRoutePage : BaseView
	{
		StartRouteViewModel ViewModel;

		public StartRoutePage()
		{
			InitializeComponent();
			Title = "Start Traveling";

			var directionsButton = new ToolbarItem("Directions", null, () =>
			{
				ViewModel.LaunchDirections();
			});

			AddToolbarItem(directionsButton);
		}

		protected override void OnBindingContextChanged()
		{
			base.OnBindingContextChanged();
			ViewModel = BindingContext as StartRouteViewModel;
			ViewModel.SetMap(MyMap);
		}
	}
}