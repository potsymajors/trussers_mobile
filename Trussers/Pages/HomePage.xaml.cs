﻿namespace Trussers
{
	public partial class HomePage : BaseView
	{
		HomeViewModel ViewModel;

		public HomePage()
		{
			InitializeComponent();

			ListView.ItemSelected += (sender, e) =>
			{
				ListView.SelectedItem = null;
			};
		}

		protected override void OnBindingContextChanged()
		{
			base.OnBindingContextChanged();
			ViewModel = BindingContext as HomeViewModel;
		}

		protected override void OnAppearing()
		{
			base.OnAppearing();
			RefreshData();
		}

		public void RefreshData()
		{
			ViewModel.RefreshData();
		}
	}
}