﻿using Xamarin.Forms;

namespace Trussers
{
	public partial class LearnMorePage : BaseView
	{
		public LearnMorePage()
		{
			InitializeComponent();
			NavigationPage.SetHasNavigationBar(this, false);
		}
	}
}