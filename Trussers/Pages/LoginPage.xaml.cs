﻿using Xamarin.Forms;

namespace Trussers
{
	public partial class LoginPage : BaseView
	{
		public LoginPage()
		{
			InitializeComponent();
			NavigationPage.SetHasNavigationBar(this, false);
		}
	}
}
