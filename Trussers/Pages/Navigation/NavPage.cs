﻿using Xamarin.Forms;

namespace Trussers
{
	public class NavPage : NavigationPage
	{
		public NavPage(Page topPage) : base(topPage)
		{
			BarTextColor = Color.White;
			BarBackgroundColor = Color.Black;
			SetTitleIcon(topPage, "titlelogo.png");
		}
	}
}