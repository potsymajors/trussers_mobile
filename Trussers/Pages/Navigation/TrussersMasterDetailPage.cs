﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using XLabs.Forms.Mvvm;

namespace Trussers
{
	public class TrussersMasterDetailPage : MasterDetailPage
	{
		TrussersMasterPage MasterPage;
		public static NavPage MasterNavPage;
		public static TrussersMasterDetailPage Instance;

		public async Task UpdatePage(Page page)
		{
			Detail = page;
		}

		public TrussersMasterDetailPage()
		{
			MasterPage = ViewFactory.CreatePage<TrussersMasterViewModel,TrussersMasterPage>() as TrussersMasterPage;

			MasterPage.MenuItemSelected += MasterMenuItemSelected;
			MasterPage.LogoutSelected += LogoutSelected;

			Master = MasterPage;

			var page = ViewFactory.CreatePage<HomeViewModel, HomePage>() as Page;
			MasterNavPage = new NavPage(page);

			Detail = MasterNavPage;

			Instance = this;

			IsPresentedChanged += (sender, e) =>
			{
				if ((sender as TrussersMasterDetailPage).IsPresented)
				{
					(Master as TrussersMasterPage).RefreshAvatar();
				}
			};

			if (Device.OS == TargetPlatform.iOS)
			{
				IsGestureEnabled = false;
			}
		}

		void LogoutSelected()
		{
			if (IsBusy)
			{
				return;
			}

			var service = new ContractorService();
			service.Logout(Session.Instance().Contractor);

			DependencyService.Get<ITokenStore>().ClearContractor();
		}

		void MasterMenuItemSelected(Type viewModelType)
		{
			if (IsBusy)
			{
				return;
			}

			Page page = new NavPage(ViewFactory.CreatePage(viewModelType) as Page);

			Detail = page;
			IsPresented = false;
		}
	}
}