﻿using System;
using Xamarin.Forms;

namespace Trussers
{
	public delegate void MenuItemSelected(Type viewModelType);
	public delegate void LogoutSelected();

	public partial class TrussersMasterPage : BaseView
	{
		public event MenuItemSelected MenuItemSelected;
		public event LogoutSelected LogoutSelected;

		public TrussersMasterPage()
		{
			Title = "Trussers";

			Icon = "menu";

			InitializeComponent();

			MenuList.ItemTemplate = new DataTemplate(() =>
			{
				var item = new MasterMenuItem();
				item.SetBinding(MasterMenuItem.TitleProperty, "Title");
				item.SetBinding(MasterMenuItem.ImageSourceProperty, "ImageSource");
				return item;
			});

			MenuList.ItemSelected += (sender, e) =>
			{
				var vm = e.SelectedItem as MasterMenuItemViewModel;

				if (vm == null)
				{
					return;
				}

				MenuList.SelectedItem = null;//disallow selection

				if (vm.ViewModelType == null)
				{
					LogoutSelected?.Invoke();
					var service = new AppService();
					service.GoToWelcomePage();
				}
				else
				{
					MenuItemSelected?.Invoke(vm.ViewModelType);
				}
			};
		}

		public void RefreshAvatar()
		{
			if (Session.Instance().Contractor != null)
			{
				UserAvatar.Source = Session.Instance().Contractor.AvatarUrl;
			}
		}
	}
}