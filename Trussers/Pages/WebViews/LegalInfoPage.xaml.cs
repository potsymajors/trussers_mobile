﻿using Xamarin.Forms;

namespace Trussers
{
	public partial class LegalInfoPage : BaseView
	{
		public LegalInfoPage()
		{
			InitializeComponent();
			NavigationPage.SetHasNavigationBar(this, true);
		}
	}
}
