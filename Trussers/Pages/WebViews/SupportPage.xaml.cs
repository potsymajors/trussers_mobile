﻿using Xamarin.Forms;

namespace Trussers
{
	public partial class SupportPage : BaseView
	{
		public SupportPage()
		{
			InitializeComponent();
			NavigationPage.SetHasNavigationBar(this, true);
		}
	}
}