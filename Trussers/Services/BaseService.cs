using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Plugin.Connectivity;
using System.Diagnostics;

namespace Trussers
{
	public abstract class BaseService<T> : IService<T> where T : EntityBase
	{
		public IObservable<T> Fetch(Func<Task<HttpResponseMessage>> fetchFunc,
										   Func<string, T> modifierFunc = null)
		{
			return Observable.Create<T>(observer =>
			   Scheduler.CurrentThread.Schedule(async o =>
			   {
				   if (CrossConnectivity.Current.IsConnected)
				   {

					   HttpResponseMessage request = null;
					   string content = null;

					   try
					   {
						   request = await fetchFunc.Invoke();
						   content = await request.Content.ReadAsStringAsync();
						   Debug.WriteLine(content);
					   }
					   catch (Exception e)
					   {
						   if (CrossConnectivity.Current.IsConnected)
						   {
							   observer.OnError(e);
						   }
						   observer.OnCompleted();
					   }

					   if (!request.IsSuccessStatusCode)
					   {
						   if (request.StatusCode != HttpStatusCode.ServiceUnavailable)
						   {
							   observer.OnError(new Exception(content));
						   }
					   }
					   else
					   {
						   Debug.WriteLine("Server content: " + content);

						   try
						   {
							   var obj = modifierFunc == null ?
								JsonConvert.DeserializeObject<T>(content) :
							   modifierFunc.Invoke(content);

							   observer.OnNext(obj);
						   }
						   catch (Exception ex)
						   {
							   Debug.WriteLine(new Exception("Error parsing JSON content: " + content));
							   observer.OnError(ex);
						   }
					   }
				   }

				   observer.OnCompleted();
			   })
			);
		}

		public IObservable<List<T>> FetchList(Func<Task<HttpResponseMessage>> fetchFunc,
													 Func<string, List<T>> modifierFunc = null)
		{
			return Observable.Create<List<T>>(observer =>
			   Scheduler.CurrentThread.Schedule(async o =>
			   {
				   if (CrossConnectivity.Current.IsConnected)
				   {
					   HttpResponseMessage request = null;
					   string content = null;

					   try
					   {
						   request = await fetchFunc.Invoke();
						   content = await request.Content.ReadAsStringAsync();
					   }
					   catch (Exception e)
					   {
						   if (CrossConnectivity.Current.IsConnected)
						   {
							   observer.OnError(e);
						   }
					   }

					   if (!request.IsSuccessStatusCode)
					   {
						   if (request.StatusCode != HttpStatusCode.ServiceUnavailable)
						   {
							   observer.OnError(new Exception(content));
						   }
					   }
					   else
					   {
						   var remoteList = modifierFunc == null ?
							   JsonConvert.DeserializeObject<List<T>>(content) :
							   modifierFunc.Invoke(content);

						   observer.OnNext(remoteList);
					   }
				   }

				   observer.OnCompleted();
			   })
			);
		}
	}
}