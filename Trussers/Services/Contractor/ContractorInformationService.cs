﻿using System;

namespace Trussers
{
	public class ContractorInformationService : BaseService<ContractorInformation>
	{
		public IObservable<ContractorInformation> UpdateContractor(ContractorUpdate contractor)
		{
			return Fetch(
				fetchFunc: async () =>
				{
				var requestUrl = ServiceConnection.ApiUrl + ServiceEndpoints.UPDATE_CONTRACTOR;
					return await ServiceConnection.PostContent(contractor, requestUrl);
				}
			);
		}
	}
}