﻿using System;
using System.Threading.Tasks;

namespace Trussers
{
	public class ContractorService : BaseService<Contractor>
	{
		public IObservable<Contractor> Login(LoginCredentials credentials)
		{
			return Fetch(
				fetchFunc: async () =>
				{
					var requestUrl = ServiceConnection.ApiUrl + ServiceEndpoints.LOGIN;
					return await ServiceConnection.PostContent(credentials, requestUrl);
				}
			);
		}

		public IObservable<Contractor> GetContractorDetails(Contractor contractor)
		{
			return Fetch(
				fetchFunc: async () =>
				{
					var requestUrl = ServiceConnection.ApiUrl + ServiceEndpoints.CONTRACTOR_DETAILS;
					return await ServiceConnection.PostContent(contractor, requestUrl);
				}
			);
		}

		public async Task Logout(Contractor contractor)
		{
			var requestUrl = ServiceConnection.ApiUrl + ServiceEndpoints.LOGOUT;
			await ServiceConnection.PostContent(contractor, requestUrl);
		}

		public async Task UpdateContractor(ContractorUpdate value)
		{
			var requestUrl = ServiceConnection.ApiUrl + ServiceEndpoints.UPDATE_CONTRACTOR;
			await ServiceConnection.PostContent(value, requestUrl);
		}
	}
}