﻿using System;

namespace Trussers
{
	public class CompleteGigService: BaseService<GigScheduleReturn>
	{
		public IObservable<GigScheduleReturn> CompleteGig(ScheduleGig contractor)
		{
			return Fetch(
				fetchFunc: async () =>
				{
					var requestUrl = ServiceConnection.ApiUrl + ServiceEndpoints.COMPLETE_GIG;
					return await ServiceConnection.PostContent(contractor, requestUrl);
				}
			);
		}
	}
}