﻿using System;

namespace Trussers
{
	public class GigDetailService: BaseService<GigDetails>
	{
		public IObservable<GigDetails> GetDetails(ScheduleGig gig)
		{
			return Fetch(
				fetchFunc: async () =>
				{
					var requestUrl = ServiceConnection.ApiUrl + ServiceEndpoints.GIG_DETAILS;
					return await ServiceConnection.PostContent(gig, requestUrl);
				}
			);
		}
	}
}