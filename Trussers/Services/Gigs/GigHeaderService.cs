﻿using System;

namespace Trussers
{
	public class GigHeaderService : BaseService<GigHeader>
	{
		public IObservable<GigHeader> GetHeader(Contractor contractor)
		{
			return Fetch(
				fetchFunc: async () =>
				{
					var requestUrl = ServiceConnection.ApiUrl + ServiceEndpoints.GIG_HEADER;
					return await ServiceConnection.PostContent(contractor, requestUrl);
				}
			);
		}
	}
}