﻿using System;

namespace Trussers
{
	public class GigScheduleReturnService : BaseService<GigScheduleReturn>
	{
		public IObservable<GigScheduleReturn> ForfeitGig(ScheduleGig contractor)
		{
			return Fetch(
				fetchFunc: async () =>
				{
					var requestUrl = ServiceConnection.ApiUrl + ServiceEndpoints.FORFEIT_GIG;
					return await ServiceConnection.PostContent(contractor, requestUrl);
				}
			);
		}

		public IObservable<GigScheduleReturn> ScheduleGig(ScheduleGig contractor)
		{
			return Fetch(
				fetchFunc: async () =>
				{
				var requestUrl = ServiceConnection.ApiUrl + ServiceEndpoints.SCHEDULE_GIG;
					return await ServiceConnection.PostContent(contractor, requestUrl);
				}
			);
		}
	}
}