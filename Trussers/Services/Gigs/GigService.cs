﻿using System;
using System.Collections.Generic;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using Newtonsoft.Json;

namespace Trussers
{
	public class GigService : BaseService<Gig>
	{
		public IObservable<List<Gig>> GetOpenGigs(Contractor contractor)
		{
			return Observable.Create<List<Gig>>(observer =>
			   Scheduler.CurrentThread.Schedule(async o =>
			   {
				   var request = await ServiceConnection.PostContent(contractor, ServiceConnection.ApiUrl +
				                                                     ServiceEndpoints.OPEN_GIGS);

				   var content = await request.Content.ReadAsStringAsync();

				   if (request.IsSuccessStatusCode)
				   {
					   var list = JsonConvert.DeserializeObject<SessionKey>(content);

					   if (list == null)
					   {
						   observer.OnCompleted();
					   }
					   else
					   {
						   observer.OnNext(list.Gigs);
					   }
				   }
				   else
				   {
					   observer.OnError(new Exception(request.StatusCode + ": " + content));
				   }

				   observer.OnCompleted();
			   })
			);
		}

		public IObservable<List<Gig>> GetGigHistory(Contractor contractor)
		{
			return Observable.Create<List<Gig>>(observer =>
			   Scheduler.CurrentThread.Schedule(async o =>
			   {
				var request = await ServiceConnection.PostContent(contractor,ServiceConnection.ApiUrl + 
				                                                          ServiceEndpoints.GIG_HISTORY);

				   var content = await request.Content.ReadAsStringAsync();

				   if (request.IsSuccessStatusCode)
				   {
					   var list = JsonConvert.DeserializeObject<SessionKey>(content);

					   if (list == null)
					   {
						   observer.OnCompleted();
					   }
					   else
					   {
						   observer.OnNext(list.Gigs);
					   }
				   }
				   else
				   {
					   observer.OnError(new Exception(request.StatusCode + ": " + content));
				   }

				   observer.OnCompleted();
			   })
			);			
		}
	}
}