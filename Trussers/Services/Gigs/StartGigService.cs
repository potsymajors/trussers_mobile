﻿using System;

namespace Trussers
{
	public class StartGigService : BaseService<StartGigDetails>
	{
		public IObservable<StartGigDetails> StartTravel(ScheduleGig gig)
		{
			return Fetch(
				fetchFunc: async () =>
				{
					var requestUrl = ServiceConnection.ApiUrl + ServiceEndpoints.START_TRAVEL;
					return await ServiceConnection.PostContent(gig, requestUrl);
				}
			);
		}

		public IObservable<StartGigDetails> StartGig(ScheduleGig gig)
		{
			return Fetch(
				fetchFunc: async () =>
				{
					var requestUrl = ServiceConnection.ApiUrl + ServiceEndpoints.START_WORKING;
					return await ServiceConnection.PostContent(gig, requestUrl);
				}
			);
		}
	}
}