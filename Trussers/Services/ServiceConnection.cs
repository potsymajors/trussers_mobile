using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;
using System.Diagnostics;

namespace Trussers
{
	public static class ServiceConnection
	{
//#if DEBUG
		public static string ApiUrl = "https://trussersservices.azurewebsites.net/api/";

//#elif RELEASE
//			public static string ApiUrl = "http://trussers.net/api/";
//#endif

		static HttpClient CreateClient ()
		{
			return new HttpClient ();
		}

		public static StringContent GetStringContentForObject(object o) =>
			new StringContent(JsonConvert.SerializeObject(o), Encoding.UTF8, "application/json");

		public static ByteArrayContent GetByteArrayContent (byte [] data) =>
			new ByteArrayContent (data);

		#region POST

		public static async Task<HttpResponseMessage> PostContent(object obj, string url)
		{
			var content = GetStringContentForObject(obj);

			using (var client = CreateClient())
			{
				var response = await client.PostAsync(url, content);
				return response;
			}
		}

		public static async Task<HttpResponseMessage> PostUserAvatar(Dictionary<string,string> keyValuePairs, object obj, string url)
		{
			var requestString = url;
			requestString = GetKeyValuePairs(keyValuePairs, requestString);

			var content = new StringContent(Convert.ToBase64String((byte[])obj));

			using (var client = CreateClient())
			{
				client.DefaultRequestHeaders.Add("Filename", "avatar.jpg");
				var response = await client.PostAsync(requestString, content);

				return response;
			}
		}

		#endregion

		#region PUT

		public static async Task<HttpResponseMessage> PutContent(object obj, string url)
		{
			try
			{
				var json = JsonConvert.SerializeObject (obj);

				var content = new StringContent (json);

				System.Diagnostics.Debug.WriteLine ("Sending content PUT " + json);

				using (var client = CreateClient ())
				{
					var response = await client.PutAsync (url, content);

					return response;
				}
			}
			catch (Exception e)
			{
				System.Diagnostics.Debug.WriteLine ("PUT EX:  + e");	
				return null;
			}
		}

		#endregion

		#region GET

		public static async Task<HttpResponseMessage> GetContent(Dictionary<string, string> keyValuePairs, string url)
		{
			var requestString = url;
			requestString = GetKeyValuePairs(keyValuePairs, requestString);

			using (var client = CreateClient())
			{
				Debug.WriteLine("Get Content: " + url);

				var response = await client.GetAsync(requestString);

				return response;
			}
		}

		public static async Task<HttpResponseMessage> GetContentByPath(string url)
		{
			Debug.WriteLine("Get Content By Path: " + url);

			using (var client = CreateClient())
			{
				var response = await client.GetAsync(url);

				return response;
			}
		}

		#endregion

		#region DELETE

		public static async Task<HttpResponseMessage> DeleteContentByPath(string url, params string[] args)
		{
			using (var client = CreateClient())
			{
				var fullUrl = url;

				foreach (var a in args)
				{
					fullUrl += $"&{a}";
				}

				var response = await client.DeleteAsync(fullUrl);

				return response;
			}
		}
		   
		#endregion

		private static string GetKeyValuePairs(Dictionary<string, string> keyValuePairs, string requestString)
		{
			if (keyValuePairs.Count > 0)
			{
				requestString += "?";
				foreach (var tuple in keyValuePairs)
				{
					requestString += tuple.Key + "=" + tuple.Value + "&";
				}

				//cleanup
				requestString = requestString.Substring(0, requestString.Length - 1);
			}

			return requestString;
		}
	}
}