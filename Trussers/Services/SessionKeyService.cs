﻿using System;

namespace Trussers
{
	public class SessionKeyService : BaseService<SessionKey>
	{
		public IObservable<SessionKey> ValidateSessionKey(string session)
		{
			return Fetch(
				fetchFunc: async () =>
				{
					var requestUrl = ServiceConnection.ApiUrl + ServiceEndpoints.SESSION_KEY_STATUS;
					return await ServiceConnection.PostContent(session, requestUrl);
				}
			);
		}
	}
}