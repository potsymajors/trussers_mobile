﻿namespace Trussers
{
	public class Session
	{
		Contractor _contractor;
		bool _allowBackButtonOnTheMap;

		private static Session _instance = new Session();
		public bool IsLoggedIn => AppPropertyHelper.GetContractor() != null;

		private Session()
		{ }

		static internal Session Instance()
		{
			return _instance;
		}

		public bool AllowBackButtonOnTheMap
		{
			get { return _allowBackButtonOnTheMap; }
			set { _allowBackButtonOnTheMap = value; }
		}

		public Contractor Contractor
		{
			get { return _contractor; }
			set { _contractor = value; }
		}

		public void Dispose()
		{
			_instance = null;
		}
	}
}