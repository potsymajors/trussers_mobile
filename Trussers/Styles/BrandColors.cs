using Xamarin.Forms;

namespace Trussers
{
	public static class BrandColors
	{		
		public static readonly Color Green = Color.FromRgb (91, 188, 92);
		public static readonly Color BlackShade = Color.FromRgb(76, 76, 76);
	}
}