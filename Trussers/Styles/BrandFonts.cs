
using Xamarin.Forms;

namespace Trussers
{
	public static class BrandFonts
	{
		#region Ebrima
		public static readonly string EbrimaBold = Device.OnPlatform("Ebrima-Bold", "KlinicSlabBold.otf#KlinicSlabBold", "Comic Sans MS");

		#endregion
	}
}
