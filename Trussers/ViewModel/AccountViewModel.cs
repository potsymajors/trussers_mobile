﻿using System.Windows.Input;
using Xamarin.Forms;
using System;
using System.ComponentModel;
using System.Threading.Tasks;

namespace Trussers
{
	public class AccountViewModel : ViewModelBase, INotifyPropertyChanged
	{
		public AccountViewModel()
		{
			GetContractor();
		}

		#region Properties

		private string _firstName;
		public string FirstName
		{
			get { return _firstName; }

			set
			{
				if (_firstName != value)
				{
					_firstName = value;
					OnPropertyChanged("FirstName");
				}
			}
		}

		private string _lastName;
		public string LastName
		{
			get { return _lastName; }

			set
			{
				if (_lastName != value)
				{
					_lastName = value;
					OnPropertyChanged("LastName");
				}
			}
		}

		private string _cellNumber;
		public string CellNumber
		{
			get { return _cellNumber; }

			set
			{
				if (_cellNumber != value)
				{
					_cellNumber = value;
					OnPropertyChanged("CellNumber");
				}
			}
		}

		private string _email;
		public string Email
		{
			get { return _email; }

			set
			{
				if (_email != value)
				{
					_email = value;
					OnPropertyChanged("Email");
				}
			}
		}

		string _oldPassword;
		public string OldPassword
		{
			get { return _oldPassword; }
			set { SetProperty(ref _oldPassword, value); }
		}

		string _newPassword;
		public string NewPassword
		{
			get { return _newPassword; }
			set { SetProperty(ref _newPassword, value); }
		}

		string _confirmPassword;
		public string ConfirmPassword
		{
			get { return _confirmPassword; }
			set { SetProperty(ref _confirmPassword, value); }
		}

		public new event PropertyChangedEventHandler PropertyChanged;

		Contractor _contractor;
		public Contractor Contractor
		{
			get { return _contractor; }

			set
			{
				if (_contractor != value)
				{
					_contractor = value;
					OnPropertyChanged("Contractor");
				}
			}
		}

		protected virtual void OnPropertyChanged(string propertyName)
		{
			if (PropertyChanged == null)
				return;
			PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}

		#endregion

		#region Commands

		Command _saveCommand;
		public ICommand SaveCommand
		{
			get
			{
				if (_saveCommand == null)
				{
					_saveCommand = new Command(SaveClick);
				}

				return _saveCommand;
			}
		}

		#endregion

		void SetSubValues()
		{
			FirstName = Contractor.FirstName;
			LastName = Contractor.LastName;
			Email = Contractor.Email;
			CellNumber = Contractor.CellNumber;
		}

		private void GetContractor()
		{
			var service = new ContractorService();
			service.GetContractorDetails(Session.Instance().Contractor).Subscribe(
				contractor =>
				{
					Contractor = contractor;
				},
				error =>
				{
					IsBusy = false;
					AlertError("An error occurred");
				},
				onCompleted: () =>
				{
					IsBusy = false;
					SetSubValues();
				}
			);
		}

		public void SaveClick()
		{

			//  All other fields are required
			//  If NewPassword != null, OldPassword must not be null

			if (string.IsNullOrEmpty(FirstName.Trim()) || string.IsNullOrEmpty(LastName.Trim()) ||
				string.IsNullOrEmpty(Email.Trim()) || string.IsNullOrEmpty(CellNumber.Trim()))
			{
				AlertError("First Name, Last Name, Email, and Cell Number are required fields.");
				return;
			}

			if (!string.IsNullOrEmpty(NewPassword) && string.IsNullOrEmpty(OldPassword)) 
			{
				AlertError("In order to change your password, you must enter your old password.");
				return;
			}

			if (NewPassword != ConfirmPassword)
			{
				AlertMessage("New passwords do not match.");
				return;
			}	
				
			Contractor.FirstName = FirstName;
			Contractor.LastName = LastName;
			Contractor.Email = Email;
			Contractor.CellNumber = CellNumber;
			Contractor.OldPassword = OldPassword;
			Contractor.NewPassword = NewPassword;

			var contractorUpdate = new ContractorUpdate();

			contractorUpdate.InContractorSessionKey = new InContractorSessionKey();
			contractorUpdate.InContractorSessionKey.SessionKey = Session.Instance().Contractor.SessionKey;
			contractorUpdate.InContractorSessionKey.ContractorID = Session.Instance().Contractor.Identifier;
			contractorUpdate.Contractor = Contractor;

			var service = new ContractorInformationService();
			service.UpdateContractor(contractorUpdate).Subscribe(
				item =>
				{
					if (item.ID != 1)
					{
						AlertError(item.Value);
					}
					else
					{
						new AppService().GoToMainPage();
					}
				},
				error =>
				{
					IsBusy = false;
					AlertError("An error occurred");
				},
				onCompleted: () =>
				{
					IsBusy = false;					
				}
			);
		}
	}
}