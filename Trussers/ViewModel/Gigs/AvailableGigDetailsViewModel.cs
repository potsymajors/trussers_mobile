﻿using System;
using System.ComponentModel;
using System.Windows.Input;
using Xamarin.Forms;

namespace Trussers
{
	public class AvailableGigDetailsViewModel : ViewModelBase, INotifyPropertyChanged
	{
		public void Initialize(Gig gig)
		{
			_gig = gig;
			GetGigDetails();
		}

		public new event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged(string propertyName)
		{
			if (PropertyChanged == null)
				return;
			PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}

		private Gig _gig;
		public Gig Gig
		{
			get { return _gig; }
			set
			{
				if (_gig != value)
				{
					_gig = value;
					OnPropertyChanged("Gig");
				}
			}
		}

		private string _startDate;
		public string StartDate
		{
			get { return _startDate; }
			set
			{
				if (_startDate != value)
				{
					_startDate = value;
					OnPropertyChanged("StartDate");
				}
			}
		}

		private string _startTime;
		public string StartTime
		{
			get { return _startTime; }
			set
			{
				if (_startTime != value)
				{
					_startTime = value;
					OnPropertyChanged("StartTime");
				}
			}
		}

		private string _location;
		public string Location
		{
			get { return _location; }
			set
			{
				if (_location != value)
				{
					_location = value;
					OnPropertyChanged("Location");
				}
			}
		}

		private string _eventType;
		public string EventType
		{
			get { return _eventType; }
			set
			{
				if (_eventType != value)
				{
					_eventType = value;
					OnPropertyChanged("EventType");
				}
			}
		}

		private string _gigType;
		public string GigType
		{
			get { return _gigType; }
			set
			{
				if (_gigType != value)
				{
					_gigType = value;
					OnPropertyChanged("GigType");
				}
			}
		}

		private string _pay;
		public string Pay
		{
			get { return _pay; }
			set
			{
				if (_pay != value)
				{
					_pay = value;
					OnPropertyChanged("Pay");
				}
			}
		}

		private string _description;
		public string Description
		{
			get { return _description; }
			set
			{
				if (_description != value)
				{
					_description = value;
					OnPropertyChanged("Description");
				}
			}
		}

		private GigDetails _gigDetails;
		public GigDetails GigDetails
		{
			get { return _gigDetails; }

			set
			{
				if (_gigDetails != value)
				{
					_gigDetails = value;
					OnPropertyChanged("GigDetails");
				}
			}
		}

		Command _acceptGigCommand;
		public ICommand AcceptGigCommand
		{
			get
			{
				if (_acceptGigCommand == null)
				{
					_acceptGigCommand = new Command(AcceptGigClick);
				}

				return _acceptGigCommand;
			}
		}

		void SetSubValues()
		{
			
			Location = GigDetails.Location;
			EventType = GigDetails.EventType;
			GigType = GigDetails.Position;
			Pay = GigDetails.Pay;
			Description = GigDetails.Description;
			StartDate = GigDetails.StartDate.ToString("dddd,  M/dd/yyyy");
			StartTime = GigDetails.StartDate.ToString("h:mm tt");
		}

		void GetGigDetails()
		{
			IsBusy = true;

			var scheduleGig = new ScheduleGig();
			scheduleGig.ContractorID = Session.Instance().Contractor.Identifier;
			scheduleGig.SessionKey = Session.Instance().Contractor.SessionKey;
			scheduleGig.GigID = Convert.ToInt32(_gig.Identifier);

			var service = new GigDetailService();
			service.GetDetails(scheduleGig).Subscribe(
				item =>
				{
					if (item == null)
					{
						AlertError("An error occurred.  Please try again.");
					}
					else
					{
						GigDetails = item;
					}
				},
				error =>
				{
					IsBusy = false;
					AlertError("An error occurred");
				},
				onCompleted: () =>
				{
					IsBusy = false;
					SetSubValues();
				}
			);
		}

		void AcceptGigClick()
		{
			var scheduleGig = new ScheduleGig();
			scheduleGig.ContractorID = Session.Instance().Contractor.Identifier;
			scheduleGig.SessionKey = Session.Instance().Contractor.SessionKey;
			scheduleGig.GigID = Convert.ToInt32(_gig.Identifier);

			var service = new GigScheduleReturnService();
			service.ScheduleGig(scheduleGig).Subscribe(
				item =>
				{
					if (item == null)
					{
						AlertError("An error occurred.  Please try again.");
					}
					else
					{
						AlertMessage(item.ScheduleStatusMessage);
						var appService = new AppService();
						appService.GoToMainPage();
					}
				},
				error =>
				{
					IsBusy = false;
					AlertError("An error occurred");
				},
				onCompleted: () =>
				{
					IsBusy = false;
				}
			);
		}
	}
}