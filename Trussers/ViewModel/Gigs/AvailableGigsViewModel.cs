﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;
using Xamarin.Forms;
using XLabs.Forms.Mvvm;

namespace Trussers
{
	public class AvailableGigsViewModel : ViewModelBase, INotifyPropertyChanged
	{
		public override void OnViewAppearing()
		{
			base.OnViewAppearing();
			GetGigHistory();
		}

		private ObservableCollection<Gig> _gigs;
		public ObservableCollection<Gig> Gigs
		{
			get { return _gigs; }

			set
			{
				if (_gigs != value)
				{
					_gigs = value;
					OnPropertyChanged("Gigs");
				}
			}
		}

		public new event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged(string propertyName)
		{
			if (PropertyChanged == null)
				return;
			PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}

		Command _openGigCommand;
		public ICommand OpenGigCommand
		{
			get
			{
				if (_openGigCommand == null)
				{
					_openGigCommand = new Command<Gig>(OpenAvailableGigCommand);
				}

				return _openGigCommand;
			}
		}

		private void OpenAvailableGigCommand(Gig gig)
		{
			if (IsBusy)
			{
				return;
			}

			var page = ViewFactory.CreatePage<AvailableGigDetailsViewModel, AvailableGigDetailsPage>(
				(vm, v) => vm.Initialize(gig)) as Page;
			
			PushToPage(page);
		}

		private void GetGigHistory()
		{
			var service = new GigService();
			service.GetOpenGigs(Session.Instance().Contractor).Subscribe(
				gigs =>
				{		
					Gigs = new ObservableCollection<Gig>();

					foreach (var item in gigs)
					{
						item.StartDateString = item.StartDate.ToString("dddd,  M/dd/yyyy");
						item.StartTimeString = item.StartDate.ToString("h:mm tt");
						Gigs.Add(item);
					}								
				},
				error =>
				{
					IsBusy = false;
					AlertError("An error occurred");
				},
				onCompleted: () =>
				{
					IsBusy = false;
				}
			);
		}
	}
}