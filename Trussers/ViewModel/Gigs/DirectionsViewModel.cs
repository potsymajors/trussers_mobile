﻿namespace Trussers
{
	public class DirectionsViewModel : ViewModelBase
	{
		public void Initialize(string directionsURL)
		{
			DrivingDirections = directionsURL;
		}

		private string _directionsURL;
		public string DrivingDirections
		{
			get { return _directionsURL; }

			set
			{
				if (_directionsURL != value)
				{
					_directionsURL = value;
				}
			}
		}
	}
}