﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace Trussers
{
	public class HistoryViewModel : ViewModelBase, INotifyPropertyChanged
	{		
		public override void OnViewAppearing()
		{
			base.OnViewAppearing();
			GetGigHistory();
		}

		private ObservableCollection<Gig> _gigs;
		public ObservableCollection<Gig> Gigs
		{
			get { return _gigs; }

			set
			{
				if (_gigs != value)
				{
					_gigs = value;
					OnPropertyChanged("Gigs");
				}
			}
		}

		public new event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged(string propertyName)
		{
			if (PropertyChanged == null)
				return;
			PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}

		private void GetGigHistory()
		{
			var service = new GigService();
			service.GetGigHistory(Session.Instance().Contractor).Subscribe(
				gigs =>
				{
					if (gigs != null)
					{
						Gigs = new ObservableCollection<Gig>();

						foreach (var item in gigs)
						{
							item.StartDateString = item.StartDate.ToString("dddd,  M/dd/yyyy");
							item.StartTimeString = item.StartDate.ToString("h:mm tt");
							Gigs.Add(item);
						}
					}					
				},
				error =>
				{
					IsBusy = false;
					AlertError("An error occurred");
				},
				onCompleted: () =>
				{
					IsBusy = false;					
				}
			);
		}
	}
}