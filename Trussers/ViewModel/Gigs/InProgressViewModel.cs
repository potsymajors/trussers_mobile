﻿using System;
using System.ComponentModel;
using System.Windows.Input;
using Xamarin.Forms;

namespace Trussers
{
	public class InProgressViewModel : ViewModelBase, INotifyPropertyChanged
	{
		public void Initialize(Gig gig)
		{
			_gig = gig;
			GetGigDetails();
		}

		public new event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged(string propertyName)
		{
			if (PropertyChanged == null)
				return;
			PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}

		private Gig _gig;
		public Gig Gig
		{
			get { return _gig; }
		}

		private GigDetails _gigDetails;
		public GigDetails GigDetails
		{
			get { return _gigDetails; }

			set
			{
				if (_gigDetails != value)
				{
					_gigDetails = value;
					OnPropertyChanged("GigDetails");
				}
			}
		}

		private string _checkInTime;
		public string CheckInTime
		{
			get { return _checkInTime; }

			set
			{
				if (_checkInTime != value)
				{
					_checkInTime = value;
					OnPropertyChanged("CheckInTime");
				}
			}
		}

		private string _position;
		public string Position
		{
			get { return _position; }

			set
			{
				if (_position != value)
				{
					_position = value;
					OnPropertyChanged("Position");
				}
			}
		}

		private string _contactPhoneNumber;
		public string ContactPhoneNumber
		{
			get { return _contactPhoneNumber; }

			set
			{
				if (_contactPhoneNumber != value)
				{
					_contactPhoneNumber = value;
					OnPropertyChanged("ContactPhoneNumber");
				}
			}
		}

		private string _contactName;
		public string ContactName
		{
			get { return _contactName; }

			set
			{
				if (_contactName != value)
				{
					_contactName = value;
					OnPropertyChanged("ContactName");
				}
			}
		}

		Command _doneCommand;
		public ICommand DoneCommand
		{
			get
			{
				if (_doneCommand == null)
				{
					_doneCommand = new Command(DoneClick);
				}

				return _doneCommand;
			}
		}

		void DoneClick()
		{
			IsBusy = true;

			var forfeitGig = new ScheduleGig();
			forfeitGig.ContractorID = Session.Instance().Contractor.Identifier;
			forfeitGig.SessionKey = Session.Instance().Contractor.SessionKey;
			forfeitGig.GigID = Convert.ToInt32(_gigDetails.Identifier);

			var service = new CompleteGigService();
			service.CompleteGig(forfeitGig).Subscribe(
				item =>
				{
					if (item == null)
					{
						AlertError("An error occurred.  Please try again.");
					}
					else 
					{					
						AlertMessage(item.ScheduleStatusMessage);						
						var appService = new AppService();
						appService.GoToMainPage();
					}					
				},
				error =>
				{
					IsBusy = false;
					AlertError("An error occurred");
				},
				onCompleted: () =>
				{
					IsBusy = false;
				}
			);
		}

		void SetSubValues()
		{
			CheckInTime = _gigDetails.CheckInDate.ToString("h:mm tt");
			Position = _gigDetails.Position;
			ContactName = _gigDetails.ContactName;
			ContactPhoneNumber = _gigDetails.ContactCell;
		}

		void GetGigDetails()
		{
			IsBusy = true;

			var scheduleGig = new ScheduleGig();
			scheduleGig.ContractorID = Session.Instance().Contractor.Identifier;
			scheduleGig.SessionKey = Session.Instance().Contractor.SessionKey;
			scheduleGig.GigID = Convert.ToInt32(_gig.Identifier);

			var service = new GigDetailService();
			service.GetDetails(scheduleGig).Subscribe(
				item =>
				{
					if (item == null)
					{
						AlertError("An error occurred.  Please try again.");
					}
					else
					{
						GigDetails = item;
					}
				},
				error =>
				{
					IsBusy = false;
					AlertError("An error occurred");
				},
				onCompleted: () =>
				{
					IsBusy = false;
					SetSubValues();
				}
			);
		}
	}
}