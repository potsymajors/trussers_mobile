﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using XLabs.Forms.Mvvm;

namespace Trussers
{
	public class StartRouteViewModel : ViewModelBase, INotifyPropertyChanged
	{
		public void Initialize(Gig gig, bool eEnroute)
		{
			_gig = gig;
			_enRoute = eEnroute;

			if (_enRoute)
			{
				GetGigDetails();
			}
			else
			{
				StartGig();
			}
		}

		//  HACK
		//  THIS SHOULD NOT BE HERE.
		public void SetMap(CustomMap map)
		{
			_map = map;
		}

		private CustomMap _map;

		private bool _enRoute;
		public bool Enroute { get; set; }

		private Gig _gig;
		public Gig Gig
		{
			get { return _gig; }
		}

		private string _startDate;
		public string StartDate
		{
			get { return _startDate; }

			set
			{
				if (_startDate != value)
				{
					_startDate = value;
					OnPropertyChanged("StartDate");
				}
			}
		}

		private string _startTime;
		public string StartTime
		{
			get { return _startTime; }

			set
			{
				if (_startTime != value)
				{
					_startTime = value;
					OnPropertyChanged("StartTime");
				}
			}
		}

		private string _location;
		public string Location
		{
			get { return _location; }

			set
			{
				if (_location != value)
				{
					_location = value;
					OnPropertyChanged("Location");
				}
			}
		}

		private string _eventType;
		public string EventType
		{
			get { return _eventType; }

			set
			{
				if (_eventType != value)
				{
					_eventType = value;
					OnPropertyChanged("EventType");
				}
			}
		}

		private string _gigType;
		public string GigType
		{
			get { return _gigType; }

			set
			{
				if (_gigType != value)
				{
					_gigType = value;
					OnPropertyChanged("GigType");
				}
			}
		}

		private double _latitude;
		public double Latitude
		{
			get { return _latitude; }

			set
			{
				if (_latitude != value)
				{
					_latitude = value;
					OnPropertyChanged("Latitude");
				}
			}
		}

		private double _longitude;
		public double Longitude
		{
			get { return _longitude; }

			set
			{
				if (_longitude != value)
				{
					_longitude = value;
					OnPropertyChanged("Longitude");
				}
			}
		}

		private string _contactName;
		public string ContactName
		{
			get { return _contactName; }

			set
			{
				if (_contactName != value)
				{
					_contactName = value;
					OnPropertyChanged("ContactName");
				}
			}
		}

		private string _contactPhoneNumber;
		public string ContactPhoneNumber
		{
			get { return _contactPhoneNumber; }

			set
			{
				if (_contactPhoneNumber != value)
				{
					_contactPhoneNumber = value;
					OnPropertyChanged("ContactPhoneNumber");
				}
			}
		}

		private string _position;
		public string Position
		{
			get { return _position; }

			set
			{
				if (_position != value)
				{
					_position = value;
					OnPropertyChanged("Position");
				}
			}
		}

		private string _arrivalInstructions;
		public string ArrivalInstructions
		{
			get { return _arrivalInstructions; }

			set
			{
				if (_arrivalInstructions != value)
				{
					_arrivalInstructions = value;
					OnPropertyChanged("ArrivalInstructions");
				}
			}
		}

		private Location LocationDetail;

		private StartGigDetails _startGigDetails;
		private GigDetails _gigDetails;
		public GigDetails GigDetails
		{
			get { return _gigDetails; }

			set
			{
				if (_gigDetails != value)
				{
					_gigDetails = value;
					OnPropertyChanged("GigDetails");
				}
			}
		}

		public new event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged(string propertyName)
		{
			if (PropertyChanged == null)
				return;
			PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}

		Command _checkInCommand;
		public ICommand CheckInCommand
		{
			get
			{
				if (_checkInCommand == null)
				{
					_checkInCommand = new Command(CheckInClick);
				}

				return _checkInCommand;
			}
		}

		void SetSubValues()
		{
			if (_enRoute)
			{
				StartTime = GigDetails.StartDate.ToString("h:mm tt");
				LocationDetail = GigDetails.LocationDetail;
				Location = LocationDetail.ToString();
				ContactName = GigDetails.ContactName;
				ArrivalInstructions = GigDetails.ArrivalInstructions;
				Position = GigDetails.Position;
				Latitude = LocationDetail.Lat;
				Longitude = LocationDetail.Lon;
				ContactPhoneNumber = GigDetails.ContactCell;
			}
			else
			{
				StartTime = _startGigDetails.GigDetails.StartDate.ToString("h:mm tt");
				LocationDetail = _startGigDetails.GigDetails.LocationDetail;
				Location = _startGigDetails.GigDetails.Location;
				ContactName = _startGigDetails.GigDetails.Pay;
				ArrivalInstructions = _startGigDetails.GigDetails.ArrivalInstructions;
				Position = _startGigDetails.GigDetails.Position;
				Latitude = LocationDetail.Lat;
				Longitude = LocationDetail.Lon;
				ContactPhoneNumber = _startGigDetails.GigDetails.ContactCell;
			}

			UpdateMap();
		}

		//  HACK
		//  THIS SHOULD NOT BE IN THE VIEW MODEL
		private void UpdateMap()
		{
			var pin = new CustomPin
			{
				Pin = new Pin
				{
					Type = PinType.Place,
					Position = new Position(Latitude, Longitude),
					Label = Location,
					Address = LocationDetail.Address
				},
				Id = "Trussers"
			};

			_map.CustomPins = new List<CustomPin> { pin };
			_map.Pins.Add(pin.Pin);

			_map.MoveToRegion(
				MapSpan.FromCenterAndRadius(new Position(Latitude, Longitude), Distance.FromMiles(1)));
		}
		void GetGigDetails()
		{
			IsBusy = true;

			var scheduleGig = new ScheduleGig();
			scheduleGig.ContractorID = Session.Instance().Contractor.Identifier;
			scheduleGig.SessionKey = Session.Instance().Contractor.SessionKey;
			scheduleGig.GigID = Convert.ToInt32(_gig.Identifier);

			var service = new GigDetailService();
			service.GetDetails(scheduleGig).Subscribe(
				item =>
				{
					if (item == null)
					{
						AlertError("An error occurred.  Please try again.");
					}
					else
					{
						GigDetails = item;
					}
				},
				error =>
				{
					IsBusy = false;
					AlertError("An error occurred");
				},
				onCompleted: () =>
				{
					IsBusy = false;
					SetSubValues();
				}
			);
		}

		public void LaunchDirections()
		{
			var page = ViewFactory.CreatePage<DirectionsViewModel, DirectionsPage>(
				(vm, v) => vm.Initialize(GigDetails.DrivingDirections)) as Page;

			PushToPage(page);
		}

		void StartGig()
		{
			var scheduleGig = new ScheduleGig();
			scheduleGig.ContractorID = Session.Instance().Contractor.Identifier;
			scheduleGig.SessionKey = Session.Instance().Contractor.SessionKey;
			scheduleGig.GigID = Convert.ToInt32(_gig.Identifier);

			var service = new StartGigService();
			service.StartTravel(scheduleGig).Subscribe(
				item =>
				{
					if (item == null || item.GigDetails == null)
					{
						if (item == null)
						{
							AlertError("An error occurred, please try again.");
						}
						else
						{
							AlertError(item.ScheduleStatusMessage);
						}		
						var appService = new AppService();
						appService.GoToMainPage();
					}
					else
					{
						_startGigDetails = item;
						SetSubValues();
					}
				},
				error =>
				{
					IsBusy = false;
					AlertError("An error occurred");
				},
				onCompleted: () =>
				{
					IsBusy = false;					
				}
			);
		}

		void CheckInClick()
		{
			IsBusy = true;

			var scheduleGig = new ScheduleGig();
			scheduleGig.ContractorID = Session.Instance().Contractor.Identifier;
			scheduleGig.SessionKey = Session.Instance().Contractor.SessionKey;
			scheduleGig.GigID = Convert.ToInt32(_gig.Identifier);

			var service = new StartGigService();
			service.StartGig(scheduleGig).Subscribe(
				item =>
				{
					if (item == null)
					{
						AlertError("An error occurred.  Please try again.");
					}
					else
					{
						var page = ViewFactory.CreatePage<InProgressViewModel, InProgressPage>(
							(vm, v) => vm.Initialize(_gig)) as Page;

						PushToPage(page);
					}
				},
				error =>
				{
					IsBusy = false;
					AlertError("An error occurred");
				},
				onCompleted: () =>
				{
					IsBusy = false;
				}
			);
		}
	}
}