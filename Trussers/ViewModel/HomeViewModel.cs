﻿using System.Collections.ObjectModel;
using System.Windows.Input;
using Xamarin.Forms;
using XLabs.Forms.Mvvm;
using System;
using System.ComponentModel;

namespace Trussers
{
	public class HomeViewModel : ViewModelBase, INotifyPropertyChanged
	{
		public HomeViewModel()
		{
			Gigs = new ObservableCollection<Gig>();
		}

		public void RefreshData()
		{
			GetGigHeader();
		}

		public GigHeader GigHeader { get; set; }
		public ObservableCollection<Gig> Gigs { get; set; }

		public new event PropertyChangedEventHandler PropertyChanged;

		private string _buttonText;
		public string ButtonText
		{
			get { return _buttonText; }

			set
			{
				if (_buttonText != value)
				{
					_buttonText = value;
					OnPropertyChanged("ButtonText");
				}
			}
		}

		private string _contentAreaText;
		public string ContentAreaText
		{
			get { return _contentAreaText; }

			set
			{
				if (_contentAreaText != value)
				{
					_contentAreaText = value;
					OnPropertyChanged("ContentAreaText");
				}
			}
		}

		private string _labelText;
		public string LabelText
		{
			get { return _labelText; }

			set
			{
				if (_labelText != value)
				{
					_labelText = value;
					OnPropertyChanged("LabelText");
				}
			}
		}

		private bool _buttonVisible;
		public bool ButtonVisible
		{
			get { return _buttonVisible; }

			set
			{
				if (_buttonVisible != value)
				{
					_buttonVisible = value;
					OnPropertyChanged("ButtonVisible");
				}
			}
		}
		protected virtual void OnPropertyChanged(string propertyName)
		{
			if (PropertyChanged == null)
				return;
			PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}

		#region Commands

		Command _gigsCommand;
		public ICommand GigsCommand
		{
			get
			{
				if (_gigsCommand == null)
				{
					_gigsCommand = new Command(GigsClicked);
				}

				return _gigsCommand;
			}
		}

		Command _openGigCommand;
		public ICommand OpenGigCommand
		{
			get
			{
				if (_openGigCommand == null)
				{
					_openGigCommand = new Command<Gig>(OpenGig);
				}

				return _openGigCommand;
			}
		}

		#endregion

		public void GigsClicked()
		{
			if (IsBusy)
			{
				return;
			}

			if (GigHeader?.ScheduledGig?.PendingGig == 1)
			{
				var scheduledGig = GigHeader?.ScheduledGig.ScheduledGigs[0];

				var page = ViewFactory.CreatePage<StartRouteViewModel, StartRoutePage>(
					(vm, v) => vm.Initialize(scheduledGig, false)) as Page;
											
				PushToPage(page);
			}
			else 
			{
				var page = ViewFactory.CreatePage<AvailableGigsViewModel, AvailableGigsPage>() as Page;
				PushToPage(page);
			}
		}

		bool CheckIfWeShouldNavigateAwayFromOurselves()
		{
			if (Session.Instance().AllowBackButtonOnTheMap)
			{
				return false;
			}

			var scheduledGigInfo = GigHeader?.ScheduledGig;

			Session.Instance().AllowBackButtonOnTheMap = true;

			if (scheduledGigInfo.EnrouteGig == 1)
			{
				var page = ViewFactory.CreatePage<StartRouteViewModel, StartRoutePage>(
					(vm, v) => vm.Initialize(scheduledGigInfo.ScheduledGigs[0], true)) as Page;

				PushToPage(page);
				return true;
			}
			else if (scheduledGigInfo.InProgressGig == 1)
			{
				var page = ViewFactory.CreatePage<InProgressViewModel, InProgressPage>(
					(vm, v) => vm.Initialize(scheduledGigInfo.ScheduledGigs[0])) as Page;

				PushToPage(page);

				return true;
			}

			return false;
		}

		void DetermineUserInterfaceElements()
		{
			ButtonVisible = IsButtonVisible();
			ButtonText = GetButtonText();
			ContentAreaText = GetContentAreaText();
			LabelText = GetLabelText();
			ShowLabel();
		}

		public bool IsButtonVisible()
		{
			if (GigHeader?.OpenGig.GigsAvailable == 1 || GigHeader?.ScheduledGig?.PendingGig == 1)
			{
				return true;
			}
			return false;
		}

		public string GetButtonText()
		{
			if (IsButtonVisible())
			{
				if (GigHeader?.ScheduledGig?.PendingGig == 1)
				{
					return "Start Travel";
				}
				else 
				{
					return "View Open Gigs";
				}
			}

			return "";
		}

		public string GetContentAreaText()
		{
			if (!GigHeader.AreThereScheduledGigs())
			{
				return GigHeader.OpenGig.FullStatus;
			}

			return "Scheduled Gigs:";
		}

		public bool ShowLabel()
		{
			if (GigHeader.AreThereScheduledGigs() && GigHeader?.ScheduledGig?.PendingGig == 0)
			{
				return true;
			}

			return false;
		}

		public string GetLabelText()
		{
			if (ShowLabel())
			{
				return GigHeader?.OpenGig?.ShortStatus;
			}

			return "";
		}

		void OpenGig(Gig gig)
		{
			if (IsBusy)
			{
				return;
			}

			//  Don't do anything if there is a gig that is ready to start
			if (IsButtonVisible() && GigHeader?.ScheduledGig?.PendingGig == 1)
			{
				return;
			}

			if (GigHeader?.ScheduledGig?.InProgressGig == 1)
			{
				var page = ViewFactory.CreatePage<InProgressViewModel, InProgressPage>(
					(vm, v) => vm.Initialize(gig)) as Page;
				PushToPage(page);
			}
			else if (GigHeader?.ScheduledGig?.EnrouteGig == 1)
			{
				var page = ViewFactory.CreatePage<StartRouteViewModel, StartRoutePage>(
					(vm, v) => vm.Initialize(gig, true)) as Page;
				PushToPage(page);
			}
			else
			{
				var page = ViewFactory.CreatePage<GigDetailViewModel, GigDetailPage>(
					(vm, v) => vm.Initialize(gig)) as Page;
				PushToPage(page);
			}

		}

		#region Service Calls
		private void GetGigHeader()
		{
			IsBusy = true;

			var service = new GigHeaderService();
			service.GetHeader(Session.Instance().Contractor).Subscribe(
				gig =>
				{
					Gigs.Clear();

					GigHeader = gig;

					var list = GigHeader.ScheduledGig.ScheduledGigs; 
					foreach (var item in list)
					{
						item.StartDateString = item.StartDate.ToString("dddd,  M/dd/yyyy");
						item.StartTimeString = item.StartDate.ToString("h:mm tt");
						Gigs.Add(item);
					}					
				},
				error =>
				{
					IsBusy = false;
					AlertError("An error occurred");
				},
				onCompleted: () =>
				{					
					IsBusy = false;

					var bCheck = CheckIfWeShouldNavigateAwayFromOurselves();

					if (!bCheck)
					{
						DetermineUserInterfaceElements();
					}													
				}
			);
		}

		#endregion
	}
}