﻿using System.Windows.Input;
using Xamarin.Forms;

namespace Trussers
{
	public class LearnMoreViewModel : ViewModelBase
	{
		Command _closeViewCommand;
		public ICommand CloseViewCommand
		{
			get
			{
				if (_closeViewCommand == null)
				{
					_closeViewCommand = new Command(ClosePage);
				}

				return _closeViewCommand;
			}
		}

		void ClosePage()
		{
			GoBack();
		}
	}
}