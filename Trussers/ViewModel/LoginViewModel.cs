﻿using System;
using System.Windows.Input;
using Xamarin.Forms;
using XLabs.Forms.Mvvm;

namespace Trussers
{
	public delegate void LoginCanceled();
	public delegate void LoginSuccess();

	public class LoginViewModel : ViewModelBase
	{
		#region Properties
		string _username;
		public string Username
		{
			get { return _username; }
			set { SetProperty(ref _username, value); }
		}

		string _password;
		public string Password
		{
			get { return _password; }
			set { SetProperty(ref _password, value); }
		}

		bool _displayError;
		public bool DisplayError
		{
			get { return _displayError; }
			set { SetProperty(ref _displayError, value); }
		}

		string _errorText;
		public string ErrorText
		{
			get { return _errorText; }
			set { SetProperty(ref _errorText, value); }
		}
		#endregion

		#region Commands

		Command _loginCommand;
		public ICommand LoginCommand
		{
			get
			{
				if (_loginCommand == null)
				{
					_loginCommand = new Command(LoginClick);
				}

				return _loginCommand;
			}
		}

		Command _learnMoreCommand;
		public ICommand LearnMoreCommand
		{
			get
			{
				if (_learnMoreCommand == null)
				{
					_learnMoreCommand = new Command(LearnMoreClick);
				}

				return _learnMoreCommand;
			}
		}

		#endregion

		private string GetAdditionalErrorText()
		{
			return "Email support at support@trussers.com if you continue to have login issues.";
		}

		public void LoginClick()
		{
			if (IsBusy)
			{
				return;
			}

			//Username = "Maurizio";
			//Username = "Majors";
			//Password = "password";

			DisplayError = false;

			if (string.IsNullOrEmpty(Username) || string.IsNullOrEmpty(Password))
			{
				AlertError("Please fill in your username and password");
				return;
			}

			IsBusy = true;


			var creds = new LoginCredentials()
			{
				Username = Username,
				Password = Password
			};

			var service = new ContractorService();
			service.Login(creds).Subscribe(
				contractor =>
				{
					if (contractor == null)
					{
						AlertError("An error occurred.  Please try again.");
					}
					else if (contractor.AllowLogin == 0)
					{
						DisplayError = true;
						ErrorText = "Login Failed\n" + contractor.LoginMessage + "\n" + GetAdditionalErrorText();						
					}
					else
					{
						contractor.Username = Username;
						
						AppPropertyHelper.PutContractor(contractor);

						Session.Instance().Contractor = contractor;
						var appService = new AppService();
						appService.GoToMainPage();
					}
				},
				error =>
				{
					IsBusy = false;
					AlertError("An error occurred");
				},
				onCompleted: () =>
				{
					IsBusy = false;
				}
			);
		}

		void LearnMoreClick()
		{
			var page = ViewFactory.CreatePage<LearnMoreViewModel, LearnMorePage>() as Page;
			PushToPage(page);
		}
	}
}