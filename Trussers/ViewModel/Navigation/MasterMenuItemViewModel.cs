﻿using System;
using Xamarin.Forms;

namespace Trussers
{
	public class MasterMenuItemViewModel
	{
		public string Title { get; set; }
		public ImageSource ImageSource { get; set; }
		public Type ViewModelType { get; set; }
	}
}