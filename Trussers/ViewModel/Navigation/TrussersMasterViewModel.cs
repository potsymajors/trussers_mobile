﻿using System.Collections.ObjectModel;

namespace Trussers
{
	public class TrussersMasterViewModel : ViewModelBase
	{
		MasterMenuItemViewModel HistoryItem, AccountItem, SupportItem, LegalInfoItem, LogoutItem;

		#region Properties
		string _avatar;
		public string Avatar
		{
			get { return _avatar; }
			set { SetProperty(ref _avatar, value); }
		}

		#endregion

		public TrussersMasterViewModel()
		{			
			MenuItems = new ObservableCollection<MasterMenuItemViewModel>
			{
				new MasterMenuItemViewModel {
					Title = "Home",
					ImageSource = "shopping_bag",
					ViewModelType = typeof (HomeViewModel)
				}
			};

			HistoryItem = new MasterMenuItemViewModel
			{
				Title = "History",
				ImageSource = "flyout_signout",
				ViewModelType = typeof(HistoryViewModel)
			};

			AccountItem = new MasterMenuItemViewModel
			{
				Title = "Account",
				ImageSource = "flyout_signout",
				ViewModelType = typeof(AccountViewModel)
			};

			SupportItem = new MasterMenuItemViewModel
			{
				Title = "Support",
				ImageSource = "flyout_signout",
				ViewModelType = typeof(SupportViewModel)
			};

			LegalInfoItem = new MasterMenuItemViewModel
			{
				Title = "Legal Info",
				ImageSource = "flyout_signout",
				ViewModelType = typeof(LegalInfoViewModel)
			};

			LogoutItem = new MasterMenuItemViewModel
			{
				Title = "Logout",
				ImageSource = "flyout_signout",
				ViewModelType = null
			};

			MenuItems.Add(HistoryItem);
			MenuItems.Add(AccountItem);
			MenuItems.Add(SupportItem);
			MenuItems.Add(LegalInfoItem);
			MenuItems.Add(LogoutItem);

		}

		public override void OnViewAppearing()
		{
			base.OnViewAppearing();
			ShouldShowHeader = true;
		}

		#region Properties

		ObservableCollection<MasterMenuItemViewModel> _menuItems;
		public ObservableCollection<MasterMenuItemViewModel> MenuItems
		{
			get { return _menuItems; }
			set { SetProperty(ref _menuItems, value); }
		}

		bool _shouldShowHeader;
		public bool ShouldShowHeader
		{
			get { return _shouldShowHeader; }
			set { SetProperty(ref _shouldShowHeader, value); }
		}


		#endregion
	}
}