using System;
﻿using System.Threading.Tasks;
using System.Windows.Input;
using Acr.UserDialogs;
using Xamarin.Forms;
using XLabs.Forms.Mvvm;

namespace Trussers
{
	public class ViewModelBase : ViewModel
	{
		#region Properties

		public bool IsLoggedIn => AppPropertyHelper.GetContractor() != null;

		#endregion

		#region Commands

		Command _backCommand;
		public ICommand BackCommand
		{
			get
			{
				if (_backCommand == null)
				{
					_backCommand = new Command (GoBack);
				}

				return _backCommand;
			}
		}

		#endregion

		#region Navigation

		public virtual void GoBack ()
		{
			PopPage ();
		}

		public void PushToPage (Page page)
		{
			//Device.BeginInvokeOnMainThread(async () => await TrussersMasterDetailPage.Instance.UpdatePage(new NavPage(page)));
			Device.BeginInvokeOnMainThread (async () => await Navigation.PushAsync (page));
		}

		public void PushModal (Page page)
		{
			Device.BeginInvokeOnMainThread (async () => await Navigation.PushModalAsync (page));
		}

		public void PopPage ()
		{
			Device.BeginInvokeOnMainThread (async () => await Navigation.PopAsync ());
		}

		public void PopModal ()
		{
			Device.BeginInvokeOnMainThread (async () => await Navigation.PopModalAsync ());
		}

		public void PushToPage<TViewModel, TView> (Action<TViewModel, TView> initializers = null)
			where TViewModel : ViewModelBase
			where TView : BaseView
		{
			var page = ViewFactory.CreatePage (initializers) as Page;
			PushToPage (page);
		}

		public void PushModal<TViewModel, TView> (Action<TViewModel, TView> initializers = null)
			where TViewModel : ViewModelBase
			where TView : BaseView
		{
			var page = ViewFactory.CreatePage (initializers) as Page;
			PushModal (page);
		}

		#endregion

		#region Alerts

		public void AlertError (string errorMessage)
		{
			IsBusy = false;
			UserDialogs.Instance.Alert (errorMessage, "Error", "OK");
		}

		public void AlertMessage(string message)
		{
			UserDialogs.Instance.Alert(message, "Information", "OK");
		}

		public async Task<bool> Confirm (string message, string confText = "Yes", string cancelText = "Cancel")
		{
			return await UserDialogs.Instance.ConfirmAsync (message, "Confirm", confText, cancelText);
		}

		public void Toast (string message, Tuple<string, Action> action = null)
		{
			var conf = new ToastConfig (message)
			{
				Duration = TimeSpan.FromSeconds (5)
			};

			if (action != null)
			{
				var toastAction = new ToastAction
				{
					Text = action.Item1,
					Action = action.Item2
				};

				conf.SetAction (toastAction);
			}

			UserDialogs.Instance.Toast (conf);
		}

		#endregion
	}
}