﻿using System;
using System.Windows.Input;
using Xamarin.Forms;
using XLabs.Forms.Mvvm;

namespace Trussers
{
	public class LegalInfoViewModel : ViewModelBase
	{
		#region Commands

		Command _showContractorCommand;
		public ICommand ShowContractorCommand
		{
			get
			{
				if (_showContractorCommand == null)
				{
					_showContractorCommand = new Command(ShowContractor);
				}

				return _showContractorCommand;
			}
		}

		Command _showAdditionalTerms;
		public ICommand ShowAdditionalTermsCommand
		{
			get
			{
				if (_showAdditionalTerms == null)
				{
					_showAdditionalTerms = new Command(ShowAdditionalTerms);
				}

				return _showAdditionalTerms;
			}
		}

		#endregion

		public void ShowContractor()
		{
			if (IsBusy)
			{
				return;
			}

			var page = ViewFactory.CreatePage<ContractorTermsViewModel, ContractorTermsOfService>() as Page;
			PushToPage(page);
		}

		public void ShowAdditionalTerms()
		{
			if (IsBusy)
			{
				return;
			}

			var page = ViewFactory.CreatePage<AdditionalTermsViewModel, AdditionalTermsPage>() as Page;
			PushToPage(page);
		}
	}
}
