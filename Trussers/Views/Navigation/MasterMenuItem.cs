﻿using Xamarin.Forms;

namespace Trussers
{
	public class MasterMenuItem : ViewCell
	{
		public Label LblTitle { get; set; }
		public Image Image { get; set; }

		public MasterMenuItem()
		{
			var stackContainer = new StackLayout
			{
				HorizontalOptions = LayoutOptions.FillAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				Orientation = StackOrientation.Horizontal,
				Padding = new Thickness(21, 0, 0, 0)
			};

			Image = new Image
			{
				Margin = new Thickness(0,120,0,0),
				HorizontalOptions = LayoutOptions.Start,
				VerticalOptions = LayoutOptions.FillAndExpand
			};

			var spacer = new ContentView
			{
				WidthRequest = 12
			};

			LblTitle = new Label
			{
				FontSize = 17,
				HorizontalOptions = LayoutOptions.StartAndExpand,
				VerticalOptions = LayoutOptions.FillAndExpand,
				VerticalTextAlignment = TextAlignment.Center
			};
			stackContainer.Children.Add(Image);
			stackContainer.Children.Add(spacer);
			stackContainer.Children.Add(LblTitle);

			View = stackContainer;
		}

		#region Bindable properties

		public static readonly BindableProperty TitleProperty =
			BindableProperty.Create("Title", typeof(string), typeof(MasterMenuItem), string.Empty, BindingMode.OneWay, null,
				(bindable, oldValue, newValue) =>
				{
					(bindable as MasterMenuItem).LblTitle.Text = newValue as string;
				}
			);

		public static readonly BindableProperty ImageSourceProperty =
			BindableProperty.Create("ImageSource", typeof(ImageSource), typeof(MasterMenuItem), null, BindingMode.OneWay, null,
				(bindable, oldValue, newValue) =>
				{
					(bindable as MasterMenuItem).Image.Source = newValue as ImageSource;
				}
			);

		public string Title
		{
			get { return (string)GetValue(TitleProperty); }
			set { SetValue(TitleProperty, value); }
		}

		public ImageSource ImageSource
		{
			get { return (ImageSource)GetValue(ImageSourceProperty); }
			set { SetValue(ImageSourceProperty, value); }
		}

		#endregion
	}
}