﻿using FFImageLoading.Forms.Touch;
using Foundation;
using ImageCircle.Forms.Plugin.iOS;
using UIKit;
using XLabs.Forms;
using XLabs.Ioc;
using XLabs.Platform.Device;

namespace Trussers.iOS
{
	[Register("AppDelegate")]
	public partial class AppDelegate : XFormsApplicationDelegate
	{
		public override bool FinishedLaunching(UIApplication app, NSDictionary options)
		{
			global::Xamarin.Forms.Forms.Init();

			CachedImageRenderer.Init();
			ImageCircleRenderer.Init();

			if (!Resolver.IsSet)
			{
				SetIoc();
			}

			LoadApplication(new App());

			return base.FinishedLaunching(app, options);
		}

		void SetIoc()
		{
			var app = new XFormsAppiOS();
			app.Init(this);

			var resolverContainer = new SimpleContainer();

			resolverContainer.Register<IDevice>(t => AppleDevice.CurrentDevice)
				.Register<IDisplay>(t => t.Resolve<IDevice>().Display);

			Resolver.SetResolver(resolverContainer.GetResolver());
		}
	}
}
