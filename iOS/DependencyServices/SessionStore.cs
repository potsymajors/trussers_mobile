﻿using Foundation;
using Newtonsoft.Json;
using Trussers.iOS;
using Xamarin.Forms;

[assembly: Dependency(typeof(SessionStore))]
namespace Trussers.iOS
{
	public class SessionStore : ITokenStore
	{
		public void StoreContractor(Contractor authToken)
		{
			var json = JsonConvert.SerializeObject(authToken);
			NSUserDefaults.StandardUserDefaults.SetString(json, AppPropertyHelper.TokenKey);
		}

		public Contractor RetrieveContractor()
		{
			var json = NSUserDefaults.StandardUserDefaults.StringForKey(AppPropertyHelper.TokenKey);

			if (!string.IsNullOrEmpty(json))
			{
				return JsonConvert.DeserializeObject<Contractor>(json);
			}
			else
			{
				return null;
			}
		}

		public void ClearContractor()
		{
			NSUserDefaults.StandardUserDefaults.RemoveObject(AppPropertyHelper.TokenKey);
		}
	}
}