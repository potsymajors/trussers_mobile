﻿using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using Trussers;
using Trussers.iOS;

[assembly: ExportRenderer(typeof(TrussersLabel), typeof(TrussersLabelRenderer))]
namespace Trussers.iOS
{
	public class TrussersLabelRenderer : LabelRenderer
	{
		TrussersLabel FormsControl;

		protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
		{
			base.OnElementChanged(e);

			if (Element != null)
			{
				FormsControl = Element as TrussersLabel;
			}

			Control.Layer.BorderWidth = 2;
			Control.Layer.BorderColor = FormsControl.BorderColor.ToCGColor();
		}
	}
}